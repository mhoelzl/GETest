// Copyright 2017 Dr. Matthias Hölzl

#include "GETest.h"
#include "GEX_RestoreMana.h"
#include "PropertyMacros.h"
#include "AttributeSet.h"
#include "AbilitySystemComponent.h"
#include "ManaAttributeSet.h"

void UGEX_RestoreMana::Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams, FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const
{
	float Delta{ 0.4f };
	UAbilitySystemComponent* ASC{ ExecutionParams.GetTargetAbilitySystemComponent() };

	float OldNone{ ASC->GetNumericAttributeBase(FGameplayAttribute(None_Prop)) };
	float OldBaseChangeOnly{ ASC->GetNumericAttributeChecked(FGameplayAttribute(BaseChangeOnly_Prop)) };
	float OldChangeOnly{ ASC->GetNumericAttributeChecked(FGameplayAttribute(ChangeOnly_Prop)) };
	float OldBoth{ ASC->GetNumericAttributeChecked(FGameplayAttribute(Both_Prop)) };
	// UE_LOG(LogTemp, Warning, TEXT("RestoreMana: Old values: %.2f %.2f %.2f %.2f"), OldNone, OldBaseChangeOnly, OldChangeOnly, OldBoth);

	ASC->SetNumericAttributeBase(FGameplayAttribute(None_Prop), OldNone + Delta);
	ASC->SetNumericAttributeBase(FGameplayAttribute(BaseChangeOnly_Prop), OldBaseChangeOnly + Delta);
	ASC->SetNumericAttributeBase(FGameplayAttribute(ChangeOnly_Prop), OldChangeOnly + Delta);
	ASC->SetNumericAttributeBase(FGameplayAttribute(Both_Prop), OldBoth + Delta);

	float NewNone{ ASC->GetNumericAttributeBase(FGameplayAttribute(None_Prop)) };
	float NewBaseChangeOnly{ ASC->GetNumericAttributeChecked(FGameplayAttribute(BaseChangeOnly_Prop)) };
	float NewChangeOnly{ ASC->GetNumericAttributeChecked(FGameplayAttribute(ChangeOnly_Prop)) };
	float NewBoth{ ASC->GetNumericAttributeChecked(FGameplayAttribute(Both_Prop)) };

	UE_LOG(LogTemp, Warning, TEXT("RestoreMana: New values: %.2f %.2f %.2f %.2f"), NewNone, NewBaseChangeOnly, NewChangeOnly, NewBoth);
}
