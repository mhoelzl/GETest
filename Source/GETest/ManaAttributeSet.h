// Copyright 2017 Dr. Matthias Hölzl

#pragma once

#include "AttributeSet.h"
#include "ManaAttributeSet.generated.h"


/**
 * Simple tests for PreAttribute(Base)Change
 */
UCLASS()
class GETEST_API UManaAttributeSet : public UAttributeSet
{
	GENERATED_BODY()

public:

	void PreAttributeBaseChange(const FGameplayAttribute& Attribute, float& NewValue) const override;
	void PreAttributeChange(const FGameplayAttribute& Attribute, float& NewValue) override;

	UPROPERTY()
	FGameplayAttributeData Mana_None;

	UPROPERTY()
	FGameplayAttributeData Mana_BaseChangeOnly;
	
	UPROPERTY()
	FGameplayAttributeData Mana_ChangeOnly;

	UPROPERTY()
	FGameplayAttributeData Mana_Both;
};
