// Copyright 2017 Dr. Matthias Hölzl

#include "GETest.h"
#include "GETestActor.h"
#include "AbilitySystemComponent.h"
#include "ManaAttributeSet.h"
#include "GameplayEffect.h"
#include "GE_RestoreMana.h"
#include "PropertyMacros.h"

AGETestActor::AGETestActor()
	: AbilitySystem{ CreateDefaultSubobject<UAbilitySystemComponent>("AbilitySystem") }
	, ManaAttributeSet{ CreateDefaultSubobject<UManaAttributeSet>("ManaAttributeSet") }
	, bUseAbilitySystemComponent{ true }
{
	static auto MeshFinder = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("StaticMesh'/Game/cube.cube'"));
	if (MeshFinder.Succeeded())
	{
		GetStaticMeshComponent()->SetStaticMesh(MeshFinder.Object);
	}

	AbilitySystem->AddDefaultSubobjectSet(ManaAttributeSet);
}

void AGETestActor::BeginPlay()
{

	if (bUseAbilitySystemComponent)
	{
		FGameplayEffectContextHandle ContextHandle;
		FGameplayEffectSpec Spec(Cast<UGameplayEffect>(UGE_RestoreMana::StaticClass()->GetDefaultObject()), ContextHandle);

		AbilitySystem->ApplyGameplayEffectSpecToSelf(Spec);
	}
	else
	{
		if (UWorld* World{ GetWorld() })
		{
			FTimerHandle TimerHandle;
			World->GetTimerManager().SetTimer(TimerHandle, this, &AGETestActor::AddMana, 0.2f, true);
		}
	}
}

void AGETestActor::AddMana()
{
	float Delta{ 0.4f };

	float OldNone{ ManaAttributeSet->Mana_None.GetCurrentValue() };
	float OldBaseChangeOnly{ ManaAttributeSet->Mana_BaseChangeOnly.GetCurrentValue() };
	float OldChangeOnly{ ManaAttributeSet->Mana_ChangeOnly.GetCurrentValue() };
	float OldBoth{ ManaAttributeSet->Mana_Both.GetCurrentValue() };

	float NewNone{ OldNone + Delta };
	None_Attr.SetNumericValueChecked(NewNone, ManaAttributeSet);
	float NewBaseChangeOnly{ OldBaseChangeOnly + Delta };
	BaseChangeOnly_Attr.SetNumericValueChecked(NewBaseChangeOnly, ManaAttributeSet);
	float NewChangeOnly{ OldChangeOnly + Delta };
	ChangeOnly_Attr.SetNumericValueChecked(NewChangeOnly, ManaAttributeSet);
	float NewBoth{ OldBoth + Delta };
	Both_Attr.SetNumericValueChecked(NewBoth, ManaAttributeSet);


	UE_LOG(LogTemp, Warning, TEXT("RestoreMana: New values: %.2f %.2f %.2f %.2f"), NewNone, NewBaseChangeOnly, NewChangeOnly, NewBoth);
}
