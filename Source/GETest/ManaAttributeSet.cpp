// Copyright 2017 Dr. Matthias Hölzl

#include "GETest.h"
#include "ManaAttributeSet.h"
#include "PropertyMacros.h"

void UManaAttributeSet::PreAttributeBaseChange(const FGameplayAttribute& Attribute, float& NewValue) const
{
	if (Attribute.GetUProperty() == BaseChangeOnly_Prop || Attribute.GetUProperty() == Both_Prop)
	{
		NewValue = FMath::Clamp(NewValue, 0.f, 2.f);
	}
}

void UManaAttributeSet::PreAttributeChange(const FGameplayAttribute& Attribute, float& NewValue)
{
	if (Attribute.GetUProperty() == ChangeOnly_Prop || Attribute.GetUProperty() == Both_Prop)
	{
		NewValue = FMath::Clamp(NewValue, -1.f, 1.f);
	}
}
