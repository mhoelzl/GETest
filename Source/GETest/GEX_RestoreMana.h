// Copyright 2017 Dr. Matthias Hölzl

#pragma once

#include "GameplayEffectExecutionCalculation.h"
#include "GEX_RestoreMana.generated.h"

/**
 * The execution calculation for the restore mana effect.
 */
UCLASS()
class GETEST_API UGEX_RestoreMana : public UGameplayEffectExecutionCalculation
{
	GENERATED_BODY()
	
public:

	void Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams, FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const;
};
