// Copyright 2017 Dr. Matthias Hölzl

#include "GETest.h"
#include "GE_RestoreMana.h"
#include "GEX_RestoreMana.h"

UGE_RestoreMana::UGE_RestoreMana()
{
	DurationPolicy = EGameplayEffectDurationType::Infinite;
	Period = FScalableFloat(0.2f);
	bExecutePeriodicEffectOnApplication = true;

	FGameplayEffectExecutionDefinition ExecutionDefinition;
	ExecutionDefinition.CalculationClass = UGEX_RestoreMana::StaticClass();
	Executions.Add(ExecutionDefinition);

}
