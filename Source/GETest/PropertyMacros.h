#pragma once

// To make the code slightly more forum friendly...
#define None_Prop FindFieldChecked<UStructProperty>(UManaAttributeSet::StaticClass(), GET_MEMBER_NAME_CHECKED(UManaAttributeSet, Mana_None))

#define BaseChangeOnly_Prop FindFieldChecked<UStructProperty>(UManaAttributeSet::StaticClass(), GET_MEMBER_NAME_CHECKED(UManaAttributeSet, Mana_BaseChangeOnly))

#define ChangeOnly_Prop FindFieldChecked<UStructProperty>(UManaAttributeSet::StaticClass(), GET_MEMBER_NAME_CHECKED(UManaAttributeSet, Mana_ChangeOnly))

#define Both_Prop FindFieldChecked<UStructProperty>(UManaAttributeSet::StaticClass(), GET_MEMBER_NAME_CHECKED(UManaAttributeSet, Mana_Both))

#define None_Attr FGameplayAttribute(None_Prop)
#define BaseChangeOnly_Attr FGameplayAttribute(BaseChangeOnly_Prop)
#define ChangeOnly_Attr FGameplayAttribute(ChangeOnly_Prop)
#define Both_Attr FGameplayAttribute(Both_Prop)
