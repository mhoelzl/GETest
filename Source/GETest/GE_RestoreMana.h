// Copyright 2017 Dr. Matthias Hölzl

#pragma once

#include "GameplayEffect.h"
#include "GE_RestoreMana.generated.h"

/**
 * 
 */
UCLASS()
class GETEST_API UGE_RestoreMana : public UGameplayEffect
{
	GENERATED_BODY()
	
public:
	UGE_RestoreMana();
	
};
