// Copyright 2017 Dr. Matthias Hölzl

#pragma once

#include "Engine/StaticMeshActor.h"
#include "GETestActor.generated.h"

class UAbilitySystemComponent;
class UManaAttributeSet;

UCLASS()
class GETEST_API AGETestActor : public AStaticMeshActor
{
	GENERATED_BODY()
	
public:
	AGETestActor();
	void BeginPlay() override;

	void AddMana();

	UPROPERTY(EditAnywhere, Category = "Abilities")
	UAbilitySystemComponent* AbilitySystem;

	UPROPERTY(EditAnywhere, Category = "Abilities")
	UManaAttributeSet* ManaAttributeSet;

	UPROPERTY(EditAnywhere, Category = "Abilities")
	bool bUseAbilitySystemComponent;
};
